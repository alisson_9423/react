import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'

import { MakeSignUp, MakeLogin, MakeSurveyList } from '@/main/factories/pages'
import { SurveyList } from '@/presentation/pages'
import { ApiContext } from '@/presentation/contexts'
import { PrivateRoute } from '@/presentation/components/private-route'
import { setCurrentAccountAdapter, getCurrentAccountAdapter } from '../adapters/current-account-adapter'

export const Router: React.FC = () => {
  return (
    <ApiContext.Provider
      value={{
        setUpdateCurrentAccount: setCurrentAccountAdapter,
        getCurrentAccount: getCurrentAccountAdapter
      }}
    >
      <BrowserRouter>
        <Routes>
          <Route path='/login' element={
            <MakeLogin />
          }
          />
          <Route path='/signup' element={
            <PrivateRoute>
              <MakeSignUp />
            </PrivateRoute>
          }
          />
          <Route path='/' element={
            <PrivateRoute>
              <MakeSurveyList />
            </PrivateRoute>
          }
          />
        </Routes>
      </BrowserRouter>
    </ApiContext.Provider>
  )
}

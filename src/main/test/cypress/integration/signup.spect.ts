import * as FormHelper from '../support/form-helper'
import * as Http from '../support/signup-mock'
import faker from 'faker'

type SimulateValidSubmit = {
  name: string
  email: string
  password: string
  passwordConfirmation: string
}

const mockSimulateDuplicateEmail = (): SimulateValidSubmit => {
  const password = faker.random.alphaNumeric(5)
  return {
    name: faker.name.findName(),
    email: 'mango@gmail.com',
    password,
    passwordConfirmation: password
  }
}
const mockSimulateSubmitValid = (): SimulateValidSubmit => {
  const password = faker.random.alphaNumeric(5)
  return {
    name: faker.name.findName(),
    email: faker.internet.email(),
    password,
    passwordConfirmation: password
  }
}

const populateFields = (values: SimulateValidSubmit): void => {
  const { name, email, password, passwordConfirmation } = values
  cy.getByTestId('name').focus().type(name)
  cy.getByTestId('email').focus().type(email)
  cy.getByTestId('password').focus().type(password)
  cy.getByTestId('passwordConfirmation').focus().type(passwordConfirmation)
}

const simulateValidSubmit = (values: SimulateValidSubmit): void => {
  populateFields(values)
  cy.getByTestId('submit').click()
}

describe('Singup', () => {
  beforeEach(() => {
    cy.visit('signup')
  })

  it('Should load with correct inital state', () => {
    cy.getByTestId('name').should('have.attr', 'readOnly')
    FormHelper.testInputStatus('name', 'Campo obrigatório')

    cy.getByTestId('email').should('have.attr', 'readOnly')
    FormHelper.testInputStatus('email', 'Campo obrigatório')

    cy.getByTestId('password').should('have.attr', 'readOnly')
    FormHelper.testInputStatus('password', 'Campo obrigatório')

    cy.getByTestId('passwordConfirmation').should('have.attr', 'readOnly')
    FormHelper.testInputStatus('passwordConfirmation', 'Campo obrigatório')

    cy.getByTestId('submit').should('have.attr', 'disabled')
    cy.getByTestId('error-wrap').should('not.have.descendants')
  })

  it('Should present error state if form is invalid', () => {
    cy.getByTestId('name').focus().type(faker.random.alphaNumeric(3))
    FormHelper.testInputStatus('name', 'Valor inválido')

    cy.getByTestId('email').focus().type(faker.random.word())
    FormHelper.testInputStatus('email', 'Valor inválido')

    cy.getByTestId('password').focus().type(faker.random.alphaNumeric(3))
    FormHelper.testInputStatus('password', 'Valor inválido')

    cy.getByTestId('passwordConfirmation').focus().type(faker.random.alphaNumeric(4))
    FormHelper.testInputStatus('passwordConfirmation', 'Valor inválido')

    cy.getByTestId('submit').should('have.attr', 'disabled')
    cy.getByTestId('error-wrap').should('not.have.descendants')
  })

  it('Should present valid state if form is valid', () => {
    const password = faker.random.alphaNumeric(5)

    cy.getByTestId('name').focus().type(faker.name.findName())
    FormHelper.testInputStatus('name')

    cy.getByTestId('email').focus().type(faker.internet.email())
    FormHelper.testInputStatus('email')

    cy.getByTestId('password').focus().type(password)
    FormHelper.testInputStatus('password')

    cy.getByTestId('passwordConfirmation').focus().type(password)
    FormHelper.testInputStatus('passwordConfirmation')

    cy.getByTestId('submit').should('not.have.attr', 'disabled')
    cy.getByTestId('error-wrap').should('not.have.descendants')
  })

  it('Should present EmailInUseError on 403', () => {
    Http.mockEmailInUseError()

    simulateValidSubmit(mockSimulateDuplicateEmail())

    FormHelper.testMainError('Esse e-mail já está em uso')
    FormHelper.testUrl('/signup')
  })

  it('Should present UnexpectedError on default erro cases', () => {
    Http.mockUnexpectedError()

    simulateValidSubmit(mockSimulateSubmitValid())

    FormHelper.testMainError('Algo de errado aconteceu. Tente novamente mais tarde.')
    FormHelper.testUrl('/signup')
  })

  it('Should present save account if valid credentials are provided', () => {
    Http.mockOk()

    simulateValidSubmit(mockSimulateSubmitValid())

    cy.getByTestId('main-error').should('not.exist')
    cy.getByTestId('spinner').should('not.exist')
    FormHelper.testUrl('/')
    FormHelper.testLocalStorageItem('account')
  })

  it('Shoud prevent mutiple submits', () => {
    Http.mockOk()

    populateFields(mockSimulateSubmitValid())
    cy.getByTestId('submit').dblclick()
    FormHelper.testHttpCallsCount(1)
  })

  it('Shoul not call submit if form is invalid', () => {
    Http.mockOk()

    cy.getByTestId('email').focus().type(faker.internet.email()).type('{enter}')
    FormHelper.testHttpCallsCount(0)
  })
})

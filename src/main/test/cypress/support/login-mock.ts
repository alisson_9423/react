import * as Helper from '../support/http-mock'
import faker from 'faker'

export const mockInvalidCrendentialsError = (): void => Helper.mockInvalidCrendentialsError(/login/)
export const mockUnexpectedError = (): void => Helper.mockUnexpectedError(/login/, 'POST')
export const mockOk = (): void => Helper.mockOk(/login/, 'POST', { accessToken: faker.datatype.uuid(), name: faker.name.findName() })

// in cypress/support/index.ts
// load type definitions that come with Cypress module
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference types="cypress" />

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example cy.dataCy('greeting')
     */
    getByTestId: typeof getByTestId
  }
}

function getByTestId (id: string): Cypress.Chainable<JQuery> {
  return cy.get(`[data-testid="${id}"]`)
}

Cypress.Commands.add('getByTestId', getByTestId)

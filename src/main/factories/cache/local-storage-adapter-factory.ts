import { LocalStorageAdapter } from '@/infra/cache/local-storage-adpater'

export const makeLocalStorageAdapter = (): LocalStorageAdapter => {
  return new LocalStorageAdapter()
}

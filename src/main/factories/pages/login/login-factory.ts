
import { EmailValidation, MinlegthValidation, RequiredFieldValidation, ValidationComposite } from '@/validation/validators'

export const makeLoginValidation = (): ValidationComposite => {
  return ValidationComposite.build([
    new RequiredFieldValidation('email'),
    new EmailValidation('email'),

    new RequiredFieldValidation('password'),
    new MinlegthValidation('password', 5)
  ])
}

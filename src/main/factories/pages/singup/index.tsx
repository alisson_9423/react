import React from 'react'
import { makeSignUpValidation } from './signup-factory'
import { SignUp } from '@/presentation/pages'
import { makeRemoteAddAccount } from '../../usecases/addAccount/remote-add-account-factory'

export const MakeSignUp: React.FC = () => {
  return <SignUp
    addAccount={makeRemoteAddAccount()}
    validation={makeSignUpValidation()}
  />
}

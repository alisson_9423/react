import { makeApiUrl } from '@/main/factories/http/api-url-factory'
import { RemoteAuthentication } from '@/data/useCases/authentication/remote-authentication'
import { makeAxiosHttpClient } from '@/main/factories/http/axios-http-client-factory'
import { AddAccount, Authentication } from '@/domain/useCases'
import { RemoteAddAccount } from '@/data/useCases/add-account/remote-add-account'

export const makeRemoteAddAccount = (): AddAccount => {
  return new RemoteAddAccount(makeApiUrl('/signup'), makeAxiosHttpClient())
}

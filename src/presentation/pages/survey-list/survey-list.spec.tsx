import React from 'react'
import { SurveyList } from '@/presentation/pages'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'
import { LoadSurveyList } from '@/domain/useCases'
import { ApiContext } from '@/presentation/contexts'
import { createMemoryHistory, MemoryHistory } from 'history'
import { Router } from 'react-router-dom'

import { mockAccountModel, mockSurveyListModel } from '@/domain/test'
import { AccessDeniedError, UnexpecError } from '@/domain/erros'
import { AccountModel } from '@/domain/models'

class LoaderSurveyListSpy implements LoadSurveyList {
  callsCount = 0;
  surveys = mockSurveyListModel();
  async loadAll (): Promise<LoadSurveyList.Model[]> {
    this.callsCount++
    return this.surveys
  }
}

type SutTypes = {
  loaderSurveyListSpy: LoaderSurveyListSpy
  history: MemoryHistory
  setUpdateCurrentAccountMock: (account: AccountModel) => void
};

const makeSut = (loaderSurveyListSpy = new LoaderSurveyListSpy()): SutTypes => {
  const history = createMemoryHistory({ initialEntries: ['/'] })
  const setUpdateCurrentAccountMock = jest.fn()

  render(
    <ApiContext.Provider value={{ setUpdateCurrentAccount: setUpdateCurrentAccountMock, getCurrentAccount: () => mockAccountModel() }}>
      <Router navigator={history} location="/login">
        <SurveyList loadSurveyList={loaderSurveyListSpy} />
      </Router>
    </ApiContext.Provider>
  )
  return {
    loaderSurveyListSpy,
    setUpdateCurrentAccountMock,
    history
  }
}

describe('SurveyList Component', () => {
  test('Should present 4 empty itens on start', async () => {
    makeSut()
    const surveyList = screen.getByTestId('survey-list')
    expect(surveyList.querySelectorAll('li:empty')).toHaveLength(4)
    expect(screen.queryByTestId('error')).not.toBeInTheDocument()
    await waitFor(() => surveyList)
  })

  test('Should call LoadSurveyList', async () => {
    const { loaderSurveyListSpy } = makeSut()
    expect(loaderSurveyListSpy.callsCount).toBe(1)
    await waitFor(() => screen.getByRole('heading'))
  })

  test('Should render SurveyItem on success', async () => {
    makeSut()
    const surveyList = screen.getByTestId('survey-list')
    await waitFor(() => {
      expect(surveyList.querySelectorAll('li.surveyItemWrap')).toHaveLength(3)
      expect(screen.queryByTestId('error')).not.toBeInTheDocument()
    })
  })

  test('Should render error on UnexpectedError', async () => {
    const loadSurveyListSpy = new LoaderSurveyListSpy()
    const error = new UnexpecError()
    jest.spyOn(loadSurveyListSpy, 'loadAll').mockRejectedValueOnce(error)

    makeSut(loadSurveyListSpy)

    await waitFor(() => {
      screen.getByRole('heading')
      expect(screen.queryByTestId('survey-list')).not.toBeInTheDocument()
      expect(screen.getByTestId('error')).toHaveTextContent(error.message)
    })
  })

  test('Should logout on AccessDeniedError', async () => {
    const loadSurveyListSpy = new LoaderSurveyListSpy()
    jest.spyOn(loadSurveyListSpy, 'loadAll').mockRejectedValueOnce(new AccessDeniedError())

    const { setUpdateCurrentAccountMock, history } = makeSut(loadSurveyListSpy)

    await waitFor(() => {
      screen.getByRole('heading')
      expect(setUpdateCurrentAccountMock).toHaveBeenCalledWith(undefined)
      expect(history.location.pathname).toBe('/login')
    })
  })

  test('Should call LoadSurveyList on reload', async () => {
    const loadSurveyListSpy = new LoaderSurveyListSpy()
    jest.spyOn(loadSurveyListSpy, 'loadAll').mockRejectedValueOnce(new UnexpecError())

    makeSut(loadSurveyListSpy)

    await waitFor(() => {
      screen.getByRole('heading')
      fireEvent.click(screen.queryByTestId('reload'))
      expect(loadSurveyListSpy.callsCount).toBe(1)
    })
  })
})

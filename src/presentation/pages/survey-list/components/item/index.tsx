import { LoadSurveyList } from '@/domain/useCases'
import { Icon, IconName } from '@/presentation/components'
import React from 'react'
import Styles from './styles.scss'

type Props = {
  survey: LoadSurveyList.Model
}

export const SurveyItem: React.FC<Props> = (props: Props) => {
  const { survey } = props
  const iconeName = survey.didAnswer ? IconName.UP : IconName.DOWN
  return (
    <li className={Styles.surveyItemWrap}>
      <div className={Styles.surveyContent}>
        <Icon iconName={iconeName} clasName={Styles.iconWrap}/>
        <time>
          <span data-testid="day" className={Styles.day}>
            {survey.date.getDate().toString().padStart(2, '0')}
          </span>
          <span data-testid="month" className={Styles.month}>
            {survey.date.toLocaleDateString('pt-BR', { month: 'short' }).replace('.', '')}
          </span>
          <span data-testid="year" className={Styles.year}>
            {survey.date.getFullYear()}
          </span>
        </time>
        <p data-testid="question">{survey.question}</p>
      </div>

      <footer>Ver Resultado</footer>
    </li>
  )
}

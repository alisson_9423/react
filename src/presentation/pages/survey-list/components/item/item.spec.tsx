import { mockSurveytModel } from '@/domain/test'
import { IconName } from '@/presentation/components'
import { SurveyItem } from '@/presentation/pages/survey-list/components'
// import { SurveyItem } from '../../components/survey-item'
import { render, screen } from '@testing-library/react'
import React from 'react'

const makeSut = (survey = mockSurveytModel()): void => {
  render(<SurveyItem survey={survey} />)
}

describe('SurveyListComponent', () => {
  test('Should render with correct values', () => {
    const survey = {
      ...mockSurveytModel(),
      didAnswer: true,
      date: new Date('2022-01-10T18:40:05.000000Z')
    }
    makeSut(survey)
    expect(screen.getByTestId('icon')).toHaveProperty('alt', IconName.UP)
    expect(screen.getByTestId('question')).toHaveTextContent(survey.question)
    expect(screen.getByTestId('day')).toHaveTextContent('10')
    expect(screen.getByTestId('month')).toHaveTextContent('jan')
    expect(screen.getByTestId('year')).toHaveTextContent('2022')
  })

  test('Should render with correct values', () => {
    const survey = {
      ...mockSurveytModel(),
      didAnswer: false,
      date: new Date('2019-05-03T18:40:05.000000Z')
    }
    makeSut(survey)
    expect(screen.getByTestId('icon')).toHaveProperty('alt', IconName.DOWN)
    expect(screen.getByTestId('question')).toHaveTextContent(survey.question)
    expect(screen.getByTestId('day')).toHaveTextContent('03')
    expect(screen.getByTestId('month')).toHaveTextContent('mai')
    expect(screen.getByTestId('year')).toHaveTextContent('2019')
  })
})

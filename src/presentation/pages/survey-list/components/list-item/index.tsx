import React, { useContext } from 'react'
import { SurveyEmpty, SurveyItem, SurveyContext } from '@/presentation/pages/survey-list/components'
import Styles from './styles.scss'
import { LoadSurveyList } from '@/domain/useCases'

export const ListItem: React.FC = () => {
  const { state } = useContext(SurveyContext)
  return <ul className={Styles.listWrap} data-testid="survey-list">
    {state.surveys.length
      ? state.surveys.map((survey: LoadSurveyList.Model) => <SurveyItem survey={survey} key={survey.id}/>)
      : <SurveyEmpty />
    }
  </ul>
}

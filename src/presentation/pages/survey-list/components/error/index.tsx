import React, { useContext } from 'react'
import { SurveyContext } from '@/presentation/pages/survey-list/components'
import Styles from './styles.scss'

export const ErrorItem: React.FC = () => {
  const { state, setState } = useContext(SurveyContext)

  const reload = (): void => {
    setState({ surveys: [], error: '', reload: !state.reload })
  }

  return (
    <div className={Styles.errorWrap}>
      <span data-testid="error">{state.error}</span>
      <button data-testid="reload" onClick={reload}>Tentar Novamanete</button>
    </div>
  )
}

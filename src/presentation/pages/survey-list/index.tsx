import React, { useEffect, useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import { FooterMemo, MemoizedHeader } from '@/presentation/components'
import { ListItem, ErrorItem, SurveyContext } from '@/presentation/pages/survey-list/components'
import Styles from './styles.scss'
import { LoadSurveyList } from '@/domain/useCases'
import { AccessDeniedError } from '@/domain/erros'
import { ApiContext } from '@/presentation/contexts'
import { useErrorHandler } from '@/presentation/hooks'

type Props = {
  loadSurveyList?: LoadSurveyList
}

export const SurveyList: React.FC<Props> = (props: Props) => {
  const { loadSurveyList } = props
  const handleError = useErrorHandler((error: Error) => {
    setState({ ...state, error: error.message })
  })

  const [state, setState] = useState({
    surveys: [] as LoadSurveyList.Model[],
    error: '',
    reload: false
  })

  useEffect(() => {
    async function fetchData () {
      try {
        const data = await loadSurveyList.loadAll()
        setState({ ...state, surveys: data })
      } catch (error) {
        handleError(error)
      }
    }

    fetchData()
  }, [state.reload])

  return (
    <div className={Styles.surveyListWrap}>
      <MemoizedHeader />

      <div className={Styles.contentWrap}>
        <h2>Enquetes</h2>
        <SurveyContext.Provider value={{ state, setState }}>
          {state.error ? <ErrorItem /> : <ListItem /> }
        </SurveyContext.Provider>
      </div>

      <FooterMemo />
    </div>
  )
}

import { Validation } from '@/presentation/protocools/validation'

export class ValidationStub implements Validation {
  erroMessage: string

  validate (fieldName: string, input: object): string {
    return this.erroMessage
  }
}

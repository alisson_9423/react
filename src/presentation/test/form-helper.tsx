import { fireEvent, screen } from '@testing-library/react'
import faker from 'faker'

export const testStatusForField = (fieldName: string, validationError: string = ''): void => {
  const wrap = screen.getByTestId(`${fieldName}-wrap`)
  const label = screen.getByTestId(fieldName)
  const field = screen.getByTestId(`${fieldName}-label`)

  expect(wrap).toHaveAttribute('data-status', validationError ? 'invalid' : 'valid')
  expect(field).toHaveProperty('title', validationError)
  expect(label).toHaveProperty('title', validationError)
}

export const populateField = (filedName: string, value = faker.random.word()): void => {
  const input = screen.getByTestId(filedName)
  fireEvent.input(input, { target: { value: value } })
}

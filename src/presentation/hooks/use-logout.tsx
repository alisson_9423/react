import { ApiContext } from '@/presentation/contexts'
import { useContext } from 'react'
import { useNavigate } from 'react-router-dom'

type ResultType = () => void

export const useLogout = (): ResultType => {
  const { setUpdateCurrentAccount } = useContext(ApiContext)
  const navigate = useNavigate()

  return (): void => {
    setUpdateCurrentAccount(undefined)
    navigate('/login')
  }
}

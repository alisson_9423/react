import React, { memo, useContext } from 'react'
import { Spinner } from '../spinner'
import Styles from './form-status.scss'
import Context from '@/presentation/contexts/form/form-context'

const FormStatus: React.FC = () => {
  const { state } = useContext(Context)
  const { isLoading, mainError } = state
  return (
    <div data-testid="error-wrap" className={Styles.errorWrap}>
      {state.isLoading && <Spinner className={Styles.spinner}/>}
      {state.mainError && <span data-testid="main-error" className={Styles.error}>{ state.mainError }</span>}
    </div>
  )
}

export const FormStatusMemo = memo(FormStatus)

import React, { memo } from 'react'
import { Logo } from '@/presentation/components/logo'
import Styles from './login-header-styles.scss'

const LoginHeader: React.FC = () => {
  return (
    <header className={Styles.headerWrap}>
      <Logo />
      <h1>4Dev - Enquentes para Programadores</h1>
    </header>
  )
}

export const LoginHeaderMemo = memo(LoginHeader)

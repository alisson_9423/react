import React from 'react'
import Styles from './icon-styles.scss'
import upImg from '../../img/up.png'
import downImg from '../../img/down.png'

export enum IconName{
  UP = 'up',
  DOWN = 'down',
}

type Props = {
  iconName: IconName
  clasName?: string
}

export const Icon: React.FC<Props> = (props: Props) => {
  const iconColor = props.iconName === IconName.UP ? Styles.green : Styles.red

  return (
    <div className={[Styles.iconWrap, iconColor, props.clasName].join(' ')}>
      <img data-testid="icon" alt={props.iconName === IconName.UP ? 'up' : 'down'} className={Styles.icon} src={props.iconName === IconName.UP ? upImg : downImg} />
    </div>
  )
}

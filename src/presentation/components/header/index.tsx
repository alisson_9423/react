import React, { MouseEvent, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import { ApiContext } from '@/presentation/contexts'
import { Logo } from '@/presentation/components/'
import Styles from './header-styles.scss'
import { useLogout } from '@/presentation/hooks'

export const Header: React.FC = () => {
  const logout = useLogout()
  const { getCurrentAccount } = useContext(ApiContext)
  const buttonClick = (e: MouseEvent<HTMLAnchorElement>): void => {
    e.preventDefault()
    logout()
  }

  return <header className={Styles.headerWrap}>
    <div className={Styles.headerContent}>
      <Logo />
      <div className={Styles.logoutWrap}>
        <span data-testid="username">{getCurrentAccount().name}</span>
        <a data-testid="logout" onClick={buttonClick} href="#">Sair</a>
      </div>
    </div>
  </header>
}

export const MemoizedHeader = React.memo(Header)

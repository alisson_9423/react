import React from 'react'
import { ApiContext } from '@/presentation/contexts'
import { createMemoryHistory, MemoryHistory } from 'history'
import { Router } from 'react-router-dom'
import { Header } from '@/presentation/components'
import { fireEvent, render, screen } from '@testing-library/react'
import { AccountModel } from '@/domain/models'
import { mockAccountModel } from '@/domain/test'

type SutTypes ={
  history: MemoryHistory
  setUpdateCurrentAccountMock: (accoun: AccountModel) => void
}

const makeSut = (account = mockAccountModel()): SutTypes => {
  const history = createMemoryHistory({ initialEntries: ['/'] })
  const setUpdateCurrentAccountMock = jest.fn()

  render(
    <ApiContext.Provider value={{ setUpdateCurrentAccount: setUpdateCurrentAccountMock, getCurrentAccount: () => account }}>
      <Router navigator={history} location="/login">
        <Header/>
      </Router>
    </ApiContext.Provider>
  )

  return { history, setUpdateCurrentAccountMock }
}

describe('HeaderComponent', () => {
  test('Should call setCurrentAccount with null', () => {
    const { setUpdateCurrentAccountMock, history } = makeSut()
    fireEvent.click(screen.getByTestId('logout'))
    expect(setUpdateCurrentAccountMock).toHaveBeenCalledWith(undefined)
    expect(history.location.pathname).toBe('/login')
  })

  test('Should render username correctly', () => {
    const account = mockAccountModel()
    makeSut(account)
    expect(screen.getByTestId('username')).toHaveTextContent(account.name)
  })
})

import React from 'react'
import { render } from '@testing-library/react'
import { Router, Routes, Route, useRoutes } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import { PrivateRoute } from '.'
import { ApiContext } from '@/presentation/contexts'
import { mockAccountModel } from '@/domain/test'

type SutTypes = {
  history: ReturnType<typeof createMemoryHistory>
}

const makeSut = (account = mockAccountModel()): SutTypes => {
  const history = createMemoryHistory({
    initialEntries: ['/']
  })
  render(
    <ApiContext.Provider value={{ getCurrentAccount: () => account }}>
      <Router location={history.location} navigator={history}>
        <Routes>
          <Route path="/" element={
            <PrivateRoute>
              <h1>opa</h1>
            </PrivateRoute>
          } />
        </Routes>
      </Router>
    </ApiContext.Provider>
  )

  return {
    history
  }
}

describe('PrivateRoute', () => {
  test('Should redirect to /login if token is empty', () => {
    const { history } = makeSut(null)
    expect(history.location.pathname).toBe('/login')
  })

  test('Should render current component if token is no empty', () => {
    const { history } = makeSut()
    expect(history.location.pathname).toBe('/')
  })
})

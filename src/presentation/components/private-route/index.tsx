import React, { useContext } from 'react'
import { Navigate } from 'react-router-dom'
import { ApiContext } from '@/presentation/contexts'

type PrivaterouteProps = {
  children: React.ReactNode
}

export const PrivateRoute: React.FC<PrivaterouteProps> = (props: PrivaterouteProps) => {
  const { children } = props
  const { getCurrentAccount } = useContext(ApiContext)

  if (!getCurrentAccount()?.accessToken) {
    return <Navigate to='/login' />
  }

  return (children as React.ReactElement)
}

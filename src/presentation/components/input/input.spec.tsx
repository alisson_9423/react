import React from 'react'
import { fireEvent, render, RenderResult } from '@testing-library/react'
import { Input } from '@/presentation/components/'
import Context from '@/presentation/contexts/form/form-context'
import faker from 'faker'

const makeSut = (fieldName: string): RenderResult => {
  return render(
    <Context.Provider value={{ state: {} }}>
      <Input name={fieldName}/>
    </Context.Provider>
  )
}

describe('Input Component', () => {
  test('Should begin with readOnly', () => {
    const filedName = faker.database.column()
    const { getByTestId } = makeSut(filedName)
    const input = getByTestId(filedName) as HTMLInputElement
    expect(input.readOnly).toBe(true)
  })

  test('Should remove readOnly on focus', () => {
    const filedName = faker.database.column()
    const { getByTestId } = makeSut(filedName)
    const input = getByTestId(filedName) as HTMLInputElement
    fireEvent.focus(input)
    expect(input.readOnly).toBe(false)
  })
})

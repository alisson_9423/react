import React, { useContext, FocusEvent, ChangeEvent } from 'react'
import Context from '@/presentation/contexts/form/form-context'
import Styles from './input-styles.scss'

type Props = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>

export const Input: React.FC<Props> = (props: Props) => {
  const { state, setState } = useContext(Context)
  const error = state[`${props.name}Error`]

  return (
    <div
      data-status={error ? 'invalid' : 'valid'}
      data-testid={`${props.name}-wrap`}
      className={Styles.inputWrap}>
      <input
        {...props}
        data-testid={props.name}
        title={error}
        placeholder=" "
        readOnly
        onFocus={event => { event.target.readOnly = false }}
        onChange={event => setState({ ...state, [event.target.name]: event.target.value })} />

      <label title={error} data-testid={`${props.name}-label`}>
        {props.placeholder}
      </label>
    </div>
  )
}

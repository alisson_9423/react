import axios from 'axios'
import faker from 'faker'

export const mockHttpReponse = (): any => ({
  data: faker.random.objectElement(),
  status: faker.datatype.number()
})

export const mockAxios = (): jest.Mocked<typeof axios> => {
  const mockedAxios = axios as jest.Mocked<typeof axios>
  mockedAxios.post.mockClear().mockResolvedValue(mockHttpReponse())
  mockedAxios.get.mockClear().mockResolvedValue(mockHttpReponse())
  return mockedAxios
}

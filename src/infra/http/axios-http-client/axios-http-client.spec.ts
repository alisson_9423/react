import { AxiosHttpClient } from './axios-http-client'
import { mockGetRequest, mockPostRequest } from '@/data/test'
import { mockAxios, mockHttpReponse } from '@/infra/test'
import axios from 'axios'

jest.mock('axios')

type SutType ={
  sut: AxiosHttpClient
  mockeAxios: jest.Mocked<typeof axios>
}

const makesut = (): SutType => {
  const sut = new AxiosHttpClient()
  const mockeAxios = mockAxios()
  return {
    sut,
    mockeAxios
  }
}

describe('AxiosHttpClient', () => {
  describe('post', () => {
    test('Should call axios.post with correct values', async () => {
      const request = mockPostRequest()
      const { sut, mockeAxios } = makesut()
      await sut.post(request)
      expect(mockeAxios.post).toHaveBeenCalledWith(request.url, request.body)
    })

    test('Should return correct on axios.post ', async () => {
      const { sut, mockeAxios } = makesut()
      const httpResponse = await sut.post(mockPostRequest())
      const axiosResponse = await mockeAxios.post.mock.results[0].value
      expect(httpResponse).toEqual({
        statusCode: axiosResponse.status,
        body: axiosResponse.data
      })
    })

    test('Should return correct error on axios.post', () => {
      const { sut, mockeAxios } = makesut()
      mockeAxios.post.mockRejectedValueOnce({
        response: mockHttpReponse()
      })
      const promise = sut.post(mockPostRequest())
      expect(promise).toEqual(mockeAxios.post.mock.results[0].value)
    })
  })

  describe('get', () => {
    test('Should call axios.get with correct values', async () => {
      const request = mockGetRequest()
      const { sut, mockeAxios } = makesut()
      await sut.get(request)
      expect(mockeAxios.get).toHaveBeenCalledWith(request.url, { headers: request.headers })
    })

    test('Should return correct on axios.get ', async () => {
      const { sut, mockeAxios } = makesut()
      const httpResponse = await sut.get(mockGetRequest())
      const axiosResponse = await mockeAxios.get.mock.results[0].value
      expect(httpResponse).toEqual({
        statusCode: axiosResponse.status,
        body: axiosResponse.data
      })
    })

    test('Should return correct error on axios.get', () => {
      const { sut, mockeAxios } = makesut()
      mockeAxios.get.mockRejectedValueOnce({
        response: mockHttpReponse()
      })
      const promise = sut.get(mockGetRequest())
      expect(promise).toEqual(mockeAxios.get.mock.results[0].value)
    })
  })
})

export interface LoadSurveyList {
  loadAll: () => Promise<LoadSurveyList.Model[]>
}

export namespace LoadSurveyList{
  export type Model = SurveyModel

  type SurveyModel = {
    id: string
    question: string
    date: Date
    didAnswer: boolean
  }

  type AnswerModel = {
    image?: string
    answer: string
  }
}

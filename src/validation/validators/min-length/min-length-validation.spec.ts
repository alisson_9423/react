import { InvalidFieldError } from '@/validation/errors'
import { MinlegthValidation } from '@/validation/validators/min-length/min-length-validation'
import faker from 'faker'

const makeSut = (field: string): MinlegthValidation => new MinlegthValidation(field, 5)

describe('MinlengthValidation', () => {
  test('Should return error if value is invalid', () => {
    const field = faker.database.column()
    const sut = makeSut(field)
    const error = sut.validate({ [field]: faker.datatype.string(4) })
    expect(error).toEqual(new InvalidFieldError())
  })
  test('Should return falsy if value is valid', () => {
    const field = faker.database.column()
    const sut = makeSut(field)
    const error = sut.validate({ [field]: faker.datatype.string(5) })
    expect(error).toBeFalsy()
  })

  test('Should return falsy if field not exists in schema', () => {
    const sut = makeSut(faker.database.column())
    const error = sut.validate({ [faker.database.column()]: faker.datatype.string(5) })
    expect(error).toBeFalsy()
  })
})

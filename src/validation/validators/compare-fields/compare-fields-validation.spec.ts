import { CompareFieldsValidation } from './compare-fields-validation'
import { InvalidFieldError } from '@/validation/errors/'
import faker from 'faker'

const makeSut = (field: string, valueToCompare: string): CompareFieldsValidation => new CompareFieldsValidation(field, valueToCompare)

describe('CompareFieldsValidation', () => {
  test('Should return error if compare is invalid', () => {
    const field = faker.database.column()
    const valueToCompare = faker.database.column()

    const sut = makeSut(field, valueToCompare)
    const error = sut.validate({
      [field]: faker.random.words(3),
      [valueToCompare]: faker.random.words(4)
    })
    expect(error).toEqual(new InvalidFieldError())
  })

  test('Should return falsy if compare is valid', () => {
    const field = faker.database.column()
    const valueToCompare = faker.database.column()
    const value = faker.datatype.string(5)
    const sut = makeSut(field, valueToCompare)

    const error = sut.validate({
      [field]: value,
      [valueToCompare]: value
    })
    expect(error).toBeFalsy()
  })
})
